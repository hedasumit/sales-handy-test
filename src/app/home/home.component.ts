import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {LocalStorageService} from '../local-storage/local-storage.service';


@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  houses:any = [{id: 1, name: 'Gryffindor', value: 'gryffindor'},
    {id: 2, name: 'Ravenclaw', value: 'ravenclaw'},
    {id: 3, name: 'Hufflepuff', value: 'hufflepuff'},
    {id: 4, name: 'Slytherin', value: 'slytherin'}];
  username:any = '';
  house:any = 'gryffindor';

  constructor(private router:Router, private localStorageService:LocalStorageService) {
  }

  ngOnInit() {
    // ngOnInit is called right after the directive's data-bound properties have been checked for the first time,
    // and before any of its children have been checked. It is invoked only once when the directive is instantiated.
  }

  joinUser() {
    const userData = {
      username: this.username,
      house: this.house
    };
    this.localStorageService.setLocalStorage('user', JSON.stringify(userData));
    this.router.navigate(['/dashboard']);
  }

}
