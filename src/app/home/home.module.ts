import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import {CustomMaterialModule} from './../custom-material/index';

@NgModule({
  imports: [FormsModule, CommonModule, RouterModule, CustomMaterialModule],
  declarations: [HomeComponent],
  exports: [HomeComponent]
})

export class HomeModule {
}
