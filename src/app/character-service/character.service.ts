import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {AppConfig} from '../app.config';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CharacterService {
  // Injecting the http client into the service
  constructor(private http: HttpClient) {
  }

  getCharactersByHouseName(houseName) {
    return this.http.get(AppConfig.API_ENDPOINT + 'characters/house/' + houseName);
  }

}
