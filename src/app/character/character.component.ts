import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from '../local-storage/local-storage.service';
import {CharacterService} from '../character-service/character.service'
import {Router} from '@angular/router';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {
  characterData:any = {};

  constructor(private localStorageService:LocalStorageService) {
  }


  ngOnInit() {
    this.characterData = this.localStorageService.getParseLocalStorage('character');
  }

getGender(){
  return this.characterData.gender.charAt(0).toUpperCase() + this.characterData.gender.slice(1);
}

}
