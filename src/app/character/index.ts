/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
 export * from './character.component';
export * from './character.route';
export * from './character.module';
