import { Route } from '@angular/router';
import { CharacterComponent } from './character.component';
import {AuthGuard} from '../auth-guard/auth-guard.middleware';


export const CharacterRoutes:Route[] = [
  {
    path: 'character',
    component: CharacterComponent,
    canActivate: [AuthGuard]
  }
];
