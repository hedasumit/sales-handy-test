import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {LocalStorageService} from '../local-storage/local-storage.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router:Router, private localStorageService:LocalStorageService) {
  }

  /**
   * Method to check weather the user is logged in or not by checking the local storage currentUser key
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {boolean}
   */
  canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot) {
    if (this.localStorageService.getParseLocalStorage('user')) {
      // logged in so return true
      return true;
    }
    else {
      // not logged in so redirect to home
      this.router.navigate(['/']);
      return false;
    }
  }
}
