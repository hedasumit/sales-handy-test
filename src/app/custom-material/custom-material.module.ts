/**
 * this module is intended to be included only required componants from angular material library
 */
import { NgModule } from '@angular/core';
import {MatButtonModule, MatCheckboxModule, MatInputModule, MatSelectModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
// add only required modules
@NgModule({
  imports: [MatButtonModule, MatCheckboxModule, MatInputModule, MatSelectModule, MatProgressSpinnerModule],
  exports: [MatButtonModule, MatCheckboxModule, MatInputModule, MatSelectModule, MatProgressSpinnerModule]
})
export class CustomMaterialModule {
}
