import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {LocalStorageService} from '../../local-storage/local-storage.service';


@Component({
  moduleId: module.id,
  selector: 'app-top-nav',
  templateUrl: 'topnav.html',
  styleUrls: ['./topnav.scss']
})

export class TopNavComponent {
  isAuthenticate = false;

  constructor(private localStorageService:LocalStorageService, private router:Router) {

    this.router.events.subscribe((val) => {
      if (this.localStorageService.getParseLocalStorage('user')) {
        // logged in so return true
        this.isAuthenticate = true;
      } else {
        this.isAuthenticate = false;
      }
    });
  }

  logout() {
    this.isAuthenticate = false;
    this.localStorageService.deleteLocalStorage('user');
    this.localStorageService.deleteLocalStorage('character');
    this.router.navigate(['/']);

  }
}
