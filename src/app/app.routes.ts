/**
 * Router file. Every module will have their own routers.
 */
import { Routes } from '@angular/router';
import { HomeRoutes } from './home/index';
import { DashboardRoutes } from './dashboard/index';
import {CharacterRoutes} from './character/index';
import {NotFoundRoutes} from './not-found/index';

export const routes:Routes = [
  ...DashboardRoutes,
  ...CharacterRoutes,
  ...NotFoundRoutes,
  ...HomeRoutes,
];
