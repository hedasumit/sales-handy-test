/**
 * Main module file
 */
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import {SharedModule} from './shared/shared.module';
import {TopNavComponent} from './shared/index';
import { HomeModule } from './home/index';
import { CustomMaterialModule } from './custom-material/index';
import { routes } from './app.routes';
import { DashboardModule } from './dashboard/index';
import {LocalStorageService} from './local-storage/local-storage.service';
import {AuthGuard} from './auth-guard/auth-guard.middleware';
import {CharacterService} from './character-service/character.service'
import { CharacterModule } from './character/index';
import { NotFoundModule } from './not-found/index'
@NgModule({
  declarations: [
    AppComponent,
    TopNavComponent
  ],
  imports: [
    FormsModule,
    HttpModule,
    RouterModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    CustomMaterialModule,
    SharedModule,
    HomeModule,
    DashboardModule,
    CharacterModule,
    NotFoundModule
  ],
  providers: [LocalStorageService, AuthGuard, CharacterService],
  bootstrap: [AppComponent],
  exports: [TopNavComponent]
})
export class AppModule { }
