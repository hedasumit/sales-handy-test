import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found.component';
import {CustomMaterialModule} from './../custom-material/index';

@NgModule({
  imports: [FormsModule, CommonModule, RouterModule, CustomMaterialModule],
  declarations: [NotFoundComponent],
  exports: [NotFoundComponent]
})

export class NotFoundModule {
}
