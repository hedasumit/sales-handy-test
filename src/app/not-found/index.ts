/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
 export * from './not-found.component';
export * from './not-found.route';
export * from './not-found.module';
