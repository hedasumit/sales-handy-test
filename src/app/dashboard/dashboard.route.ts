import { Route } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import {AuthGuard} from '../auth-guard/auth-guard.middleware';


export const DashboardRoutes:Route[] = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  }
];
