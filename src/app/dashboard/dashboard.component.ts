import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from '../local-storage/local-storage.service';
import {CharacterService} from '../character-service/character.service'
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  userData:any = {};
  houseCharacters:any = [];

  constructor(private localStorageService:LocalStorageService,
              private characterService:CharacterService,
              private router:Router) {
  }

  getALlCharacterOfHouse() {
    this.characterService.getCharactersByHouseName(this.userData.house).subscribe((response) => {
      this.houseCharacters = response;
    }, (err) => {
      console.log(err);
    });
  }

  ngOnInit() {
    this.userData = this.localStorageService.getParseLocalStorage('user');
    this.getALlCharacterOfHouse();
  }

  setCharacterDetails(character) {
    this.localStorageService.setLocalStorage('character', JSON.stringify(character));
    this.router.navigate(['/character']);
  }

  getHouseName(){
    return this.userData.house.charAt(0).toUpperCase() + this.userData.house.slice(1);
  }



}
