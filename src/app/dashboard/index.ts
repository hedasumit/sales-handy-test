/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
 export * from './dashboard.component';
export * from './dashboard.route';
export * from './dashboard.module';
