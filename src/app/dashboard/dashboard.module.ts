import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import {CustomMaterialModule} from './../custom-material/index';

@NgModule({
  imports: [FormsModule, CommonModule, RouterModule, CustomMaterialModule],
  declarations: [DashboardComponent],
  exports: [DashboardComponent]
})

export class DashboardModule {
}
